/*
 * initctld - imitate /run/initctl for non-sysvinit systems
 * Copyright (C) 2020 Arsen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <initreq.h>
#include <unistd.h>
#include <string.h>
#include <syslog.h>
#include <stdarg.h>
#include <stdlib.h>
#include <signal.h>
#include <errno.h>
#include <assert.h>

#ifndef HANDLER_DIR
#	define HANDLER_DIR "/etc/initctld"
#endif

#define ARR_LEN(x) (sizeof((x)) / sizeof((x[0])))

static int          ctlfd       = -1;
static int          verbose     = 0;
static volatile int interrupted = 0;
static const char  *handler_dir = HANDLER_DIR;

static const struct {
	const char *name;
	int facility;
} facs[] = {
	{ "auth",      LOG_AUTH     },
	{ "authpriv",  LOG_AUTHPRIV },
	{ "cron",      LOG_CRON     },
	{ "daemon",    LOG_DAEMON   },
	{ "ftp",       LOG_FTP      },
	{ "kern",      LOG_KERN     },
	{ "local0",    LOG_LOCAL0   },
	{ "local1",    LOG_LOCAL1   },
	{ "local2",    LOG_LOCAL2   },
	{ "local3",    LOG_LOCAL3   },
	{ "local4",    LOG_LOCAL4   },
	{ "local5",    LOG_LOCAL5   },
	{ "local6",    LOG_LOCAL6   },
	{ "local7",    LOG_LOCAL7   },
	{ "lpr",       LOG_LPR      },
	{ "mail",      LOG_MAIL     },
	{ "news",      LOG_NEWS     },
	{ "syslog",    LOG_SYSLOG   },
	{ "user",      LOG_USER     },
	{ "uucp",      LOG_UUCP     },
};

static int get_facility(const char *name) {
	for (size_t i = 0; i < ARR_LEN(facs); i++) {
		if (!strcmp(name, facs[i].name)) {
			return facs[i].facility;
		}
	}
	return LOG_KERN;
}

static void (*log_msg)(int, const char *, ...) = &syslog;

static void signal_handler(int sig) {
	interrupted = sig;
}

static void print_about() {
	fprintf(stderr,
		"initctld - imitate /run/initctl for non-sysvinit systems\n"
		"License: GNU General Public License, Version 3\n"
		"This is free software: you are free to change and redistribute it.\n"
		"There is NO WARRANTY, to the extent permitted by law.\n"
	       );
}

static void print_help() {
	print_about();
	fprintf(stderr,
		"Usage:\n"
		"initctld [-hv] [-p path] [-f facility] [-d reqdir]\n"
		"\t -h: print help (this message)\n"
		"\t -v: send (verbose) output to stderr\n"
		"\t -f: select syslog facility"
		" (auth, ..., local0, ..., local7)\n"
		"\t -p: set fifo path (default: " INIT_FIFO ", will be made)\n"
		"\t -d: handler directory (default: " HANDLER_DIR ")\n"
		"\t -V: information about the program\n"
	       );
}

static const char *prio_to_string(int prio) {
	switch (prio) {
	case LOG_EMERG:   return "EMERG";
	case LOG_ALERT:   return "ALERT";
	case LOG_CRIT:    return "CRIT";
	case LOG_ERR:     return "ERROR";
	case LOG_WARNING: return "WARN";
	case LOG_NOTICE:  return "NOTIC";
	case LOG_INFO:    return "INFO";
	case LOG_DEBUG:   return "DEBUG";
	default: break;
	}
	assert(!"Unknown priority");
	return "";
}

static void syslog_stderr(int prio, const char *fmt, ...) {
	va_list va;
	fprintf(stderr, "%-5s: ", prio_to_string(prio));
	va_start(va, fmt);
	vfprintf(stderr, fmt, va);
	va_end(va);
	puts("");
}

static char file_path[PATH_MAX] = { 0 };
/* static char req_data[sizeof(((struct init_request *)0xdead)->i.data) + 1]; */
static void process_request(struct init_request *req) {
	/* TODO(arsen): varying number of args based on data */
	const char *cmd;
	pid_t pid;
	char sleep_time[16];
	char runlvl[2] = { (char)req->runlevel, 0 };
	char *argv[] = {
		file_path,
		runlvl,
		sleep_time,
		0
	};
	log_msg(LOG_DEBUG, "received request %d", req->cmd);

	pid = fork();
	if (pid < 0) {
		log_msg(LOG_ERR, "fork() failed: %s (%d)",
			strerror(errno), errno);
		return;
	}
	if (pid != 0) {
		return;
	}

	switch (req->cmd) {
	case INIT_CMD_START:        cmd = "start";        break;
	case INIT_CMD_RUNLVL:       cmd = "runlvl";       break;
	case INIT_CMD_POWERFAIL:    cmd = "powerfail";    break;
	case INIT_CMD_POWERFAILNOW: cmd = "powerfailnow"; break;
	case INIT_CMD_POWEROK:      cmd = "powerok";      break;
	case INIT_CMD_SETENV:       cmd = "setenv";       break;
	case INIT_CMD_UNSETENV:     cmd = "unsetenv";     break;
	}

	/*
	 * memset(req_data, 0, sizeof(req_data));
	 * strncpy(req_data, req->i.data, sizeof(req_data));
	 */

	snprintf(file_path, sizeof(file_path), "./%s", cmd);
	snprintf(sleep_time, sizeof(sleep_time), "%d", req->sleeptime);

	execv(file_path, argv);
	if (errno == ENOENT) {
		exit(0);
	}
	log_msg(LOG_ERR, "execv() failed for %s: %s (%d)",
		file_path, strerror(errno), errno);
	exit(1);
}

static int open_fifo(int ctlfd, const char *fifo) {
	if (ctlfd > 0) {
		return ctlfd;
	}
	while ((ctlfd = open(fifo, O_RDONLY | O_CLOEXEC)) < 0) {
		if (errno != EINTR) {
			break;
		} else if (interrupted) {
			return -1;
		}
	}
	if (ctlfd < 0) {
			log_msg(LOG_ERR, "could not open %s: %s (%d)",
				fifo, strerror(errno), errno);
	}
	return ctlfd;
}

int main(int argc, char **argv) {
	const char *fifo = INIT_FIFO;
	int opt;
	int facility = LOG_DAEMON;
	struct init_request req;

	struct sigaction signal_handler_action = {
		.sa_handler = &signal_handler
	};
	struct sigaction ignore_action = {
		.sa_handler = SIG_IGN
	};

	sigemptyset(&signal_handler_action.sa_mask);
	sigemptyset(&ignore_action.sa_mask);

	while ((opt = getopt(argc, argv, "hvp:d:f:V")) != -1) {
		switch(opt) {
		default:
		case 'h':
			print_help();
			return 0;
		case 'v':
			verbose = 1;
			log_msg = &syslog_stderr;
			break;
		case 'V':
			print_about();
			return 0;
		case 'p':
			fifo = optarg;
			break;
		case 'f':
			facility = get_facility(optarg);
			break;
		case 'd':
			handler_dir = optarg;
			break;
		}
	}

	if (facility == LOG_KERN) {
		fprintf(stderr,
			"Invalid facility (%s)\n"
			"Available facilities:\n",
			optarg);
		for (size_t i = 0; i < ARR_LEN(facs); i++) {
			fprintf(stderr, "\t%s %c", facs[i].name,
				((i + 1) % 4 == 0 ? '\n' : (
					strlen(facs[i].name) == 8 ? ' ' : '\t'
					)));
		}
		return 1;
	}

	if (!verbose) {
		openlog(argv[0], LOG_PID, facility);
	}

	if (mkfifo(fifo, 0600) < 0) {
		log_msg(LOG_ERR, "mkfifo failed: %s (%d)",
			strerror(errno), errno);
		return 2;
	}

	if (chdir(handler_dir)) {
		log_msg(LOG_ERR, "could not change into handler dir: %s (%d)",
			strerror(errno), errno);
		return 5;
	}

	sigaction(SIGCHLD, &ignore_action, NULL);
	sigaction(SIGHUP, &signal_handler_action, NULL);
	sigaction(SIGINT, &signal_handler_action, NULL);
	sigaction(SIGTERM, &signal_handler_action, NULL);
	sigaction(SIGUSR1, &signal_handler_action, NULL);
	sigaction(SIGUSR2, &signal_handler_action, NULL);
	while (!interrupted && (ctlfd = open_fifo(ctlfd, fifo)) >= 0) {
		ssize_t cnt = read(ctlfd, &req, sizeof(req));
		if (cnt < 0) {
			if (errno == EINTR) {
				if (interrupted) {
					break;
				} else {
					continue;
				}
			}
			log_msg(LOG_ERR, "read() failure: %s (%d)",
				strerror(errno), errno);
			break;
		} else if (cnt == 0) {
			close(ctlfd);
			ctlfd = -2;
			continue;
		}
		if (cnt != sizeof(req)) {
			log_msg(LOG_DEBUG, "wrong init request size (%ld, expected %lu)",
				cnt, sizeof(req));
			continue;
		}
		if (req.magic != INIT_MAGIC) {
			log_msg(LOG_DEBUG, "request magic value wrong (%x, should be %x)",
				req.magic, INIT_MAGIC);
			continue;
		}

		process_request(&req);
	}

	if (unlink(fifo) < 0) {
		log_msg(LOG_ERR, "couldn't clean up pipe");
		return 3;
	}

	if (interrupted) {
		log_msg(LOG_NOTICE, "Received signal %d (%s), exiting...",
			interrupted, strsignal(interrupted));
		return 0;
	}
	return 4;
}
