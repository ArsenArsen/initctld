# installation tree prefix
# files will be installed as such:
# - initctld into $(PREFIX)/bin/
# - initctld.8 into $(PREFIX)/share/man/man8/
PREFIX = /usr/local
DEFINES = -D_POSIX_C_SOURCE=200809L
# normal cflags
CFLAGS = -std=c99 -Wall -Wextra -Wpedantic -Werror
# indev flags
DEBUG =

initctld: build/main.o
	$(CC) $(LFLAGS) -o $@ $<

build/%.o: %.c
	@mkdir -p build
	$(CC) $(DEFINES) $(DEBUG) $(PROD) $(CFLAGS) -c -o $@ $<

.PHONY: install clean help
install: initctld
	install -D initctld $(PREFIX)/bin/initctld
	install -D initctld.8 $(PREFIX)/share/man/man8/initctld.8

clean:
	-rm -r build
	-rm initctld

help:
	@echo "Targets:"
	@echo "	initctld (default):"
	@echo "		builds the daemon itself (with flags, constructed from DEFINES, DEBUG and CFLAGS: $(DEFINES) $(DEBUG) $(CFLAGS))"
	@echo "	install: installs to PREFIX ($(PREFIX))"
